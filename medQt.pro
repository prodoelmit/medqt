TEMPLATE = app


QT += qml quick serialport
CONFIG += c++11  draeger console omron
SOURCES += main.cpp \
    driver.cpp \
    app.cpp \
    measurements.cpp \
    apiadaptor.cpp

RESOURCES += qml.qrc


# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    driver.h \
    app.h \
    measurements.h \
    apiadaptor.h


contains(CONFIG, omron) {
        message( "Configuring with OMRON" )
        HEADERS += pdll.h\
                omrondll.h \
                omronadaptor.h
        SOURCES += omrondll.cpp \
                omronadaptor.cpp
        DEFINES += OMRON
        DEFINES -= UNICODE
}



contains(CONFIG, draeger) {
        message("Configuring with DRAEGER" )
#        win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../draeger_libs/bin/32-bit/ -L$$PWD/../draeger_libs/lib/32-bit/ -lScreenerComm
#        else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../draeger_libs/bin/32-bit/ -L$$PWD/../draeger_libs/lib/32-bit/ -lScreenerComm
#        INCLUDEPATH += G:/med/draeger_libs/include
#        DEPENDPATH += G:/med/draeger_libs/include
        SOURCES += \
            draegeradaptor.cpp \
            draegermeasurement.cpp
        HEADERS += \
            draegeradaptor.h \
            draegermeasurement.h
        DEFINES += DRAEGER
}

