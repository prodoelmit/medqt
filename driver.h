#ifndef DRIVER_H
#define DRIVER_H
#include <QPixmap>
#include <QObject>


class Driver: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString surname READ surname WRITE setSurname NOTIFY surnameChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString patrName READ patrName WRITE setPatrName NOTIFY patrNameChanged)
    Q_PROPERTY(int birthYear READ birthYear WRITE setBirthYear NOTIFY birthYearChanged)
    Q_PROPERTY(int persNumber READ persNumber WRITE setPersNumber NOTIFY persNumberChanged)
    Q_PROPERTY(QString avatar READ avatar WRITE setAvatar NOTIFY avatarChanged)

public:
    Q_INVOKABLE
    Driver();
    ~Driver();
    QString surname() const;
    QString name() const;
    QString patrName() const;
    int birthYear() const;
    int persNumber() const;

    QString avatar() const;

public slots:
    void setSurname(QString surname);
    void setName(QString name);
    void setPatrName(QString patrName);
    void setBirthYear(int birthYear);
    void setPersNumber(int persNumber);

    void setAvatar(QString avatar);

signals:
    void surnameChanged(QString surname);
    void nameChanged(QString name);
    void patrNameChanged(QString patrName);
    void birthYearChanged(int birthYear);
    void persNumberChanged(int persNumber);
    void avatarChanged(QString avatar);

private:
    QString m_surname;
    QString m_name;
    QString m_patrName;
    int m_birthYear;
    int m_persNumber;
    QString m_avatar;
};

#endif // DRIVER_H
