import QtQuick 2.0
import QtQuick.Layouts 1.1

ChangeScreenButton {


    id: noButton
    icon: ""
    property var topColor : "#eaeaea";
    property var bottomColor : "#ffffff";
    anchors {
        left: parent.left
        leftMargin: 16
    }
    gradient: Gradient {
        GradientStop {
            position: 1
            color: mouseArea.pressed ? bottomColor : topColor
        }

        GradientStop {
            position: 0
//            color: "#ffffff"
            color: mouseArea.pressed ? topColor : bottomColor
        }
    }


    Item {
        id: labelAndIconWrapper
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        implicitWidth: labelText.width + iconText.width + labelText.anchors.leftMargin


        Text {
            id: iconText
            visible: text.length > 0
            color: "#065980"
            text: noButton.icon
            anchors.verticalCenter: parent.verticalCenter
            font.family: "FontAwesome"
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        }

        Text {
            id: labelText
            color: "#065980"
            text: noButton.label
            anchors.verticalCenter: parent.verticalCenter
        font.family: "PTSans"
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
            anchors.left: iconText.right
            anchors.leftMargin: (iconText.visible ? 16 : 0)
        }


    }

    
}
