import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

Window {
    id: window
    property color whiteColor: "#ffffff"
    property color grayColor: "#d3d8dd"
    property color darkGrayColor: "#e9eef3"
    signal startedFromScratch
    visible: true
    visibility: Window.FullScreen
    width: 1280
    height: 848
    title: "Медосмотр"
    FontLoader  {
        id: ptsans
        name: "PTSans"
        source: "fonts/PTS55F.ttf"
    }
    FontLoader  {
        id: ptsansbold
        name: "PTSansBold"
        source: "fonts/PTS75F.ttf"
    }

    FontLoader {
        id: fontAwesome
        name: "FontAwesome"
        source: "fonts/fontawesome-webfont.ttf"
    }

    function startFromScratch(){
        startedFromScratch()
        pageWrapper.header.setCurrent("first")
        pageWrapper.stack.pop(null)
        app.abortMeasurements();
    }
    function loadDriver(id) {
        app.fetchDriver(id)

    }
    function startInspection() {

        driverConfirmationPage.visible = false
        bloodPressurePage.visible = true

    }
    function finishBloodPressureMeasurements() {
        console.log("Blood pressure measurements completed")

    }

//    Component {
//        id: firstPage
//        FirstPage {}
//    }

//    Component {
//        id: confirmationPage
//        DriverConfirmationPage {}
//    }

    Page {
        visible: true
        id: pageWrapper
//        Component.onCompleted: loadDriver(25);

    }

//    Controls.StackView {
//        id: stackView
//        anchors.fill: parent
//        initialItem: firstPage

//    }





}
