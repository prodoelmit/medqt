#ifndef APIADAPTOR_H
#define APIADAPTOR_H
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include "measurements.h"
#include "driver.h"

#define AUTHORIZATIONSTRING "ESMC-Token pKCDvY6YixmvUnou5Wnc3YiP5Cs3YWfIX159cxFBftp3bwjHPzoU4PRiMw47UkBzbpJmDvoA7rpdqarvV6Jx1ydC3qLreEXy4aco"

class ApiAdaptor: public QObject
{
    Q_OBJECT
public:
    ApiAdaptor();
    void postMeasurementResults(Measurements* m);

    bool fetchDriverInfo(Driver* driver);

signals:
    void driverInfoFetched();
    void error(QString errorString);

private:
    QNetworkAccessManager* m_manager;
    QNetworkRequest* m_postMeasurementResultRequest;
    QNetworkRequest* m_postMeasurementResultRequestbin;
};


#endif // APIADAPTOR_H
