#include "apiadaptor.h"
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QPointer>
#include <QDate>
#include <app.h>

ApiAdaptor::ApiAdaptor()
{
    m_manager = new QNetworkAccessManager();
    m_postMeasurementResultRequest = new QNetworkRequest();
    m_postMeasurementResultRequest->setUrl(QUrl("https://test.esmc.info/med/api/checkups"));
    m_postMeasurementResultRequest->setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
//    m_postMeasurementResultRequest->setHeader(QNetworkRequest::ContentDispositionHeader, "form-data");

    m_postMeasurementResultRequestbin = new QNetworkRequest();
    m_postMeasurementResultRequestbin->setUrl(QUrl("http://requestb.in/t2yxazt2"));
    m_postMeasurementResultRequestbin->setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
//    m_postMeasurementResultRequestbin->setHeader(QNetworkRequest::ContentDispositionHeader, "form-data");
}

void ApiAdaptor::postMeasurementResults(Measurements *m)
{

    QByteArray ba = m->serialize().toUtf8();

    QNetworkReply* laletinReply = m_manager->post(*m_postMeasurementResultRequest, ba);
    connect(laletinReply, &QNetworkReply::readyRead, [laletinReply]() {
       qDebug() << "Laleting replied: " <<  laletinReply->readAll();
    });

    m_manager->post(*m_postMeasurementResultRequestbin, ba);


}

bool ApiAdaptor::fetchDriverInfo(Driver *driver)
{
    int id = driver->persNumber();
    QPointer<Driver> driverptr(driver);

    qDebug() << "Supports ssl: " << QSslSocket::supportsSsl();
    qDebug() << m_manager->supportedSchemes() ;

    auto url = QUrl(QString("https://test.esmc.info/auth/api/v1/user/%1").arg(QString::number(id)) );
//    auto url = QUrl(QString("http://dev.morodeer.ru/avatar.png"));
    qDebug() << url.toString();
    QNetworkRequest req(url);

    req.setRawHeader("Authorization", AUTHORIZATIONSTRING);


    QNetworkReply* reply = m_manager->get(req);
    QObject::connect(reply, &QNetworkReply::finished, [=]() {
        qDebug() << "Reply finished";
        qDebug() << driverptr;
        if (driverptr) {

            QByteArray ba = reply->readAll();
            qDebug() << "Got reply: " << ba.length() << QString(ba);
            QJsonParseError err;
            QJsonDocument doc = QJsonDocument::fromJson(ba, &err);
            QString e = "Не удалось найти пользователя с таким номером";
            if (err.error != QJsonParseError::NoError) {
                emit error(e);
                return;
            }
            if (!doc.isObject()) {
                emit error(e);
            }
            QJsonObject obj = doc.object();
            if (!obj["user"].isObject()) {
                emit error(e);
                return;
            }
            QJsonObject userObj = obj["user"].toObject();
            if (!userObj["id"].isDouble()) {
                emit error(e);
                return;
            }
            int newId = userObj["id"].toInt();

            if (id != newId) {
                emit error(e);
                return;
            }

            driver->setName(userObj["first_name"].toString());
            driver->setPatrName(userObj["middle_name"].toString());
            driver->setSurname(userObj["last_name"].toString());
            driver->setBirthYear(QDate::fromString(userObj["birthday"].toString(), "yyyy-MM-dd").year());
//            driver->setBirthYear(1990);
            driver->setAvatar(userObj["photo_url"].toString());
//            driver->setAvatar("qrc:/avatar.png");


            emit driverInfoFetched();
        }

    });

    return true;

}
