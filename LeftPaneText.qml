import QtQuick 2.0

import QtQuick 2.0
import QtQuick.Layouts 1.1

Item {
    id: leftPaneText

    width: leftPane.width
    anchors.left: leftPane.left
    property var labels: []
    property var values: []
    property int lineheight: 26
    property int topbotmargin: 30
    property int leftrightmargin: 14
    
    height: labels.length * lineheight + 2 * topbotmargin

    function getMaxWidth(layout) {
        var max = 0;

        var children = layout.children
        for (var i = 0; i < children.length; i++) {
            var w = children[i].width;
            if (w > max) {
                max = w;
            }
        }

        console.log("Max width: " +  max);
        return max;


    }

    Row {
        id: row
     anchors.verticalCenter: parent.verticalCenter
     anchors.horizontalCenter: parent.horizontalCenter


        ColumnLayout {

            id: leftLayout
            height: labels.length * lineheight
            Layout.column: 0
            Component.onCompleted: {
                width = getMaxWidth(leftLayout)
            }

            Repeater {
                model: labels.length

                Rectangle {
                    objectName: "leftLayoutRectangle"
                    color: "transparent";
                    height: lineheight
                    Layout.row: index
                    width: text.width
                    anchors.right: parent.right
                    Text {
                        anchors.right: parent.right

                        id: text
                        text: labels[index] + ":"
                        color: "white"
                        horizontalAlignment: Text.AlignRight
                        font.family: "PTSans"
                        font.pixelSize: 14
                    }
                }

            }
        }

        ColumnLayout {
            id: rightLayout
            height: labels.length * lineheight
            Layout.column: 1
            Component.onCompleted: {
                width = getMaxWidth(rightLayout)
            }

            Repeater {
                model: labels.length
                Rectangle {
                    objectName: "RightLayoutRectangle"
                    color: "transparent"
                    height: lineheight
                    Layout.row: index
                    width: valueText.width + valueText.anchors.leftMargin
                    Text {
                        anchors.left: parent.left
                        anchors.leftMargin: 14
                        id: valueText
                        text: values[index]
                        color: "white"
                        horizontalAlignment: Text.AlignLeft
                        font.family: "PTSans"
                        font.pixelSize: 14
                    }
                }
            }

        }

    }

}
