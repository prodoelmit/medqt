#ifndef DRAEGERMEASUREMENT_H
#define DRAEGERMEASUREMENT_H
#include <QString>


class DraegerMeasurement
{
public:




    DraegerMeasurement(QString hex);
    DraegerMeasurement(const DraegerMeasurement& m);

    enum TestResultFlags {
        TestNoFlag = 0x00
        , TestPassed = 0x01
        , TestAlert = 0x02
        , TestFailed = 0x04
        , TestFailedHighBAC = 0x08
        , TestNoAlcoholPassive = 0x10
        , TestAlcoholPassive = 0x20
    };
    QString inspect();
    static QString reverseEndianness(QString s);
    float hexToFloat(QString hex);
    int id() const;

    qreal alcohol() const;

    bool passed() const;
private:
    QString m_hex;
    int m_id;
    TestResultFlags m_flags;
    qreal m_alcohol;
};

#endif // DRAEGERMEASUREMENT_H
