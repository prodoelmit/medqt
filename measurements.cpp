#include "measurements.h"
#include <QDateTime>
#include "app.h"
#include <QTimer>

Measurements::Measurements(Driver *driver)
  : m_driver(driver)
  , m_heartRateStatus(0)
  , m_alcoholMeasurementStatus(0)
  , m_step(1)
  , m_sysPressure(-1)
  , m_diaPressure(-1)
  , m_heartRate(-1)
  , m_hasComplaints(false)
{

}

void Measurements::clear()
{
    setStep(3);
    setAlcoholMeasurementStatus(0);
    setHeartRateStatus(0);

    setSysPressure(-1);
    setDiaPressure(-1);
    setHeartRate(-1);
}

QString Measurements::serialize()
{
    QJsonObject obj
    {
        {"checkedAt", m_datetime.toMSecsSinceEpoch() / 1000},
        {"complaints", m_hasComplaints},
        {"dia", m_diaPressure},
        {"driverId", m_driver->persNumber()},
        {"ppm", m_alcohol},
        {"pulse", m_heartRate},
        {"sys", m_sysPressure},
        {"type", m_inOut ? 10 : 20}
    };
            QJsonDocument doc(obj);
            return doc.toJson();
}

bool Measurements::readOmronData()
{
    setHeartRateStatus(1);
    QTimer::singleShot(0, [this](){
        int sys;
        int dia;
        int hr;
        if (app->omron()->readData(sys, dia, hr) == 0) {
        setSysPressure(sys);
        setDiaPressure(dia);
        setHeartRate(hr);
//        setSysPressure(0);
//        setDiaPressure(0);
//        setHeartRate(0);

        setHeartRateStatus(2);
        } else {
            emit error("Не удалось считать показания. Повторите запрос.");
            setHeartRateStatus(-1);
        }
    });
    return 0;
}

bool Measurements::readDraegerData() {
    setAlcoholMeasurementStatus(1);
    QTimer::singleShot(0, [this](){
        QPair<int, qreal> pair = app->draeger()->readLastAlcoholValue();
        int lastMeasurementId = pair.first;
        qreal alcohol = pair.second;
        if (lastMeasurementId <= app->currentAlcoholTestsCounter()) {
            setAlcoholMeasurementStatus(-1);
            emit error("Произошла ошибка. Повторите измерение.");
        } else {
            app->setCurrentAlcoholTestsCounter(lastMeasurementId);
            setAlcohol(alcohol);
            setAlcoholMeasurementStatus(2);
        }
    });


    return true;
}

void Measurements::finish()
{
    // post results

    // after posting start new session


    app->api()->postMeasurementResults(this);
    app->startFromScratch();


}

bool Measurements::inOut() const
{
    return m_inOut;
}

qreal Measurements::alcohol() const
{
    return m_alcohol;
}

int Measurements::sysPressure() const
{
    return m_sysPressure;
}

int Measurements::diaPressure() const
{
    return m_diaPressure;
}

QDateTime Measurements::datetime() const
{
    return m_datetime;
}

bool Measurements::hasComplaints() const
{
    return m_hasComplaints;
}

Driver *Measurements::driver() const
{
    return m_driver;
}

int Measurements::heartRate() const
{
    return m_heartRate;
}

void Measurements::startMeasurements()
{
    m_datetime = QDateTime::currentDateTime();
}

int Measurements::heartRateStatus() const
{
    return m_heartRateStatus;
}

int Measurements::alcoholMeasurementStatus() const
{
    return m_alcoholMeasurementStatus;
}

void Measurements::setInOut(bool inOut)
{
    if (m_inOut == inOut)
        return;

    m_inOut = inOut;
    emit inOutChanged(inOut);
}

void Measurements::setAlcohol(qreal alcohol)
{
    if (m_alcohol == alcohol)
        return;

    m_alcohol = alcohol;
    emit alcoholChanged(alcohol);
}

void Measurements::setSysPressure(int sysPressure)
{
    if (m_sysPressure == sysPressure)
        return;

    m_sysPressure = sysPressure;
    emit sysPressureChanged(sysPressure);
}

void Measurements::setDiaPressure(int diaPressure)
{
    if (m_diaPressure == diaPressure)
        return;

    m_diaPressure = diaPressure;
    emit diaPressureChanged(diaPressure);
}

void Measurements::setDatetime(QDateTime datetime)
{
    if (m_datetime == datetime)
        return;

    m_datetime = datetime;
    emit datetimeChanged(datetime);
}

void Measurements::setHasComplaints(bool hasComplaints)
{
    if (m_hasComplaints == hasComplaints)
        return;

    m_hasComplaints = hasComplaints;
    emit hasComplaintsChanged(hasComplaints);
}

void Measurements::setDriver(Driver *driver)
{
    if (m_driver == driver)
        return;

    m_driver = driver;
    emit driverChanged(driver);
}

void Measurements::setHeartRate(int heartRate)
{
    if (m_heartRate == heartRate)
        return;

    m_heartRate = heartRate;
    emit heartRateChanged(heartRate);
}

void Measurements::setHeartRateStatus(int heartRateStatus)
{
    if (m_heartRateStatus == heartRateStatus)
        return;

    m_heartRateStatus = heartRateStatus;
    emit heartRateStatusChanged(heartRateStatus);
}

void Measurements::setAlcoholMeasurementStatus(int alcoholMeasurementStatus)
{
    if (m_alcoholMeasurementStatus == alcoholMeasurementStatus)
        return;

    m_alcoholMeasurementStatus = alcoholMeasurementStatus;
    emit alcoholMeasurementStatusChanged(alcoholMeasurementStatus);
}
