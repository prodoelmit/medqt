import QtQuick 2.0
import QtQuick.Layouts 1.1

TabIdButton {
    id: tabIdActionButton
    property string icon: ""
    property string label: "Подтвердить"
    property var inactiveColor: "#068918"
    property var activeColor: "#eaeaea"
    color: mouseArea.pressed ? activeColor : inactiveColor
    property var action;

    Item {
        id: contentWrapper
        implicitHeight: iconText.height + labelText.height + labelText.anchors.topMargin
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        Text {
            id: iconText
            text: tabIdActionButton.icon
            verticalAlignment: Text.AlignTop
            anchors.top: parent.top
            anchors.topMargin: 0
            color: "white"
            font.pixelSize: 28
            font.family: "FontAwesome"
            width: parent.width
            horizontalAlignment: Text.AlignHCenter
            
        }
        
        Text {
            id: labelText
            width: parent.width
            text: tabIdActionButton.label
            color: "white"
            font.pixelSize: 12
            anchors {
                top: iconText.bottom
            }
            font.family: "PTSans"
            horizontalAlignment: Text.AlignHCenter
            
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: action()
    }

    
    
}
