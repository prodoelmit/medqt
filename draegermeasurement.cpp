#include "draegermeasurement.h"
#include "qdebug.h"
#include <QtEndian>
#include <QStack>
#include <QTextStream>

DraegerMeasurement::DraegerMeasurement(QString hex)
    : m_hex(hex)
{
    m_id = m_hex.mid(2,2).toInt(nullptr, 16);
    m_flags = (TestResultFlags)m_hex.mid(40,2).toInt(nullptr, 16);
    QString alcHexR = reverseEndianness(m_hex.mid(20,8));
    float alcR = hexToFloat(alcHexR);
    m_alcohol = alcR;
    if (m_alcohol < 1e-30) {
        m_alcohol = 0;
    }

}

DraegerMeasurement::DraegerMeasurement(const DraegerMeasurement &m)
    : m_id(m.m_id)
    , m_hex(m.m_hex)
    , m_flags(m.m_flags)
    , m_alcohol(m.m_alcohol)
{

}

QString DraegerMeasurement::inspect()
{
    return QString("id: %1, flags: %2, alc: %3 \t\tcontent: %4")
            .arg(QString::number(m_id))
            .arg(m_flags)
            .arg(m_alcohol)
            .arg(m_hex);

}

QString DraegerMeasurement::reverseEndianness(QString s)
{
    QStack<QString> bytes;
    for (int i = 0; i < s.length(); i += 2) {
        bytes.push(s.mid(i, 2));
    }

    QString reversed;
    while (!bytes.empty()) {
        reversed += bytes.pop();
    }
    return reversed;

}

float DraegerMeasurement::hexToFloat(QString hex)
{
    bool ok;
    int sign = 1;
    QByteArray array(hex.toLatin1());
    array = QByteArray::number(array.toLongLong(&ok,16),2); //convert hex to binary -you don't need this since your incoming data is binary
    if(array.length()==32) {
        if(array.at(0)=='1')  sign =-1;                       // if bit 0 is 1 number is negative
        array.remove(0,1);                                     // remove sign bit
    }
    QByteArray fraction =array.right(23);   //get the fractional part
    double mantissa = 0;
    for(int i=0;i<fraction.length();i++)     // iterate through the array to claculate the fraction as a decimal.
        if(fraction.at(i)=='1')     mantissa += 1.0/(pow(2,i+1));
    int exponent = array.left(array.length()-23).toLongLong(&ok,2)-127;     //claculate the exponent
    float result = sign*pow(2,exponent)*(mantissa+1.0);
    return result;
}

int DraegerMeasurement::id() const
{
    return m_id;
}

qreal DraegerMeasurement::alcohol() const
{
    return m_alcohol;
}

bool DraegerMeasurement::passed() const
{

    return (m_flags == TestPassed || m_flags == TestNoFlag);
}
