#ifndef DRAEGERADAPTOR_H
#define DRAEGERADAPTOR_H
#include <QObject>
#include <QtSerialPort/QtSerialPort>
#include "draegermeasurement.h"


class DraegerAdaptor:public QObject

{
    Q_OBJECT
public:
    DraegerAdaptor();
    bool startPCMode();
    bool initComPort();
    bool endPCMode();
    bool readMeasurements(int measurementsCount);
    int readNumberOfTests();
    int getLastMeasurementId();
    QPair<int, qreal> readLastAlcoholValue();
public slots:
    void fetchResults();
private:
    void appendCRC(QString& str);
    QSerialPort* m_port;
    QList<DraegerMeasurement> m_measurements;
protected:
    QString sendData(QString hex, int expectedLength, int timeoutMsecs);
    QString readData(int expectedLength, int timeoutMsecs);

};

#endif // DRAEGERADAPTOR_H
