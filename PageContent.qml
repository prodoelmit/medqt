import QtQuick 2.0

import QtQuick 2.0
import QtQuick.Layouts 1.1

Rectangle {
    property var finishLoading: function(){}
    property alias h1: h1.text
    property alias contents: contentsLoader.sourceComponent
    property alias contentsLoader: contentsLoader
    property alias yesButtonText: yesButton.label
    property alias noButtonText: noButton.label
    property alias yesButton: yesButton
    property alias noButton: noButton
    property alias leftPaneVisible: leftPane.visible
    id: pageContent
    color: "white"

    function showError(text) {
        h1Error.text = text
        h1ErrorWrapper.state = "hidden"
        h1ErrorWrapper.state = "visible"
    }


    Connections {
        target: app
        onError: {
            console.log("app onError")
        }

    }

    Item {
        anchors.fill: parent
        LeftPane {
            id: leftPane
        }
        Item {
            id: rightPane
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom

            anchors.left: (leftPane.visible? leftPane.right : parent.left)

            Rectangle {
                id: h1Wrapper
                width: parent.width
                color: "#e9eef3"
                anchors.top: parent.top
                anchors.topMargin: 24
                implicitHeight: h1.height

                Text {
                    id: h1
                    width: parent.width
                    height: 80
                    anchors {
                        horizontalCenter: parent.horizontalCenter
                    }
                    horizontalAlignment: Text.AlignHCenter
                    text: "Введите ваш табельный номер"
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    anchors.horizontalCenterOffset: 0
                    verticalAlignment: Text.AlignVCenter
                    font.family: "PTSans"
                    font.pixelSize: 20
                }

            }
            Rectangle {
                opacity: 0
                id: h1ErrorWrapper
                width: parent.width
                color: "#b54e4e"
                anchors.top: parent.top
                anchors.topMargin: 24
                implicitHeight: h1.height

                Text {
                    id: h1Error
                    width: parent.width
                    height: 80
                    anchors {
                        horizontalCenter: parent.horizontalCenter
                    }
                    color: "white"
                    horizontalAlignment: Text.AlignHCenter
                    text: "Введите ваш табельный номер"
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    anchors.horizontalCenterOffset: 0
                    verticalAlignment: Text.AlignVCenter
                    font.family: "PTSans"
                    font.pixelSize: 20
                }

                states: [
                State {
                        name: "hidden"
//                        PropertyChanges { target: h1ErrorWrapper; opacity: 0 }
                },
                State {
                        name: "visible"
//                        PropertyChanges { target: h1ErrorWrapper; opacity: 1 }
                }
                ]


                transitions: [
                    Transition {
                        to: "visible"
                        SequentialAnimation {
                        PropertyAnimation {
                            target: h1ErrorWrapper
                            properties: "opacity"
                            from: 0
                            to : 1
                            duration: 100
                        }
                        PropertyAnimation {
                            target: h1ErrorWrapper
                         duration: 4000
                         properties: "opacity"
                         to: 1
                        }
                    PropertyAnimation {
                        target: h1ErrorWrapper
                        duration: 1000
                        properties: "opacity"
                        to: 0

                    }
                        }
                        onRunningChanged: {
                            console.log(running)
                            console.log(state)
                            if ((!running))
                                state = "hidden"
                        }
                    }

                ]

            }


            //    Rectangle {
            //        id: pageContentsContents
            //       anchors.verticalCenter: parent.verticalCenter
            //       anchors.horizontalCenter: parent.horizontalCenter
            ////       anchors.fill: parent
            //       color: "#ffffff"
            //   }



            Component {
                id: redRect
                Rectangle {
                    color: "red"
                    anchors.fill: parent
                }
            }

            Loader {
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                //            anchors.top: h1Wrapper.bottom
                //            anchors.bottom: buttonsZone.top

                //            width: parent.width
                anchors.horizontalCenterOffset: 0
                anchors.topMargin: 0
                anchors.bottomMargin: 0
                id: contentsLoader
                sourceComponent: redRect
            }




            Item {
                id: buttonsZone
                implicitHeight: yesButton.height + yesButton.anchors.bottomMargin
                anchors {
                    bottom: parent.bottom
                }
                width: parent.width



                NoButton {
                    id: noButton
                    label: "Назад"
                    icon: ""
                }

                YesButton {
                    id: yesButton
                    label: "Продолжить"
                }

            }
    }
    }
}
