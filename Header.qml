import QtQuick 2.0
import QtQuick.Layouts 1.1

Rectangle {
    property int styleFurther: 0
    property int styleCurrent: 1
    property int stylePassed: 2
    property int styleFailed: 3

    id: header
    anchors.top: parent.top
    anchors.topMargin: 16
    color: "white"
    height: 60
    width: pageWrapper.width

    function setStyle(name, style) {
        for (var i = 0; i < breadcrumbsModel.count; i++) {
            var ch = breadcrumbsModel.get(i);
            if (ch.name === name) {
                ch.style = style
                break
            }
        }
    }

    function setCurrent(name) {
        var found = false;
        for (var i = 0; i < breadcrumbsModel.count; i++) {
            var ch = breadcrumbsModel.get(i);
            if (found) {
                ch.style = header.styleFurther
            } else if (ch.name === name) {
                ch.style = header.styleCurrent
                found = true
            }
        }
    }

    function setPassed(name) {
        setStyle(name, header.stylePassed)
    }

    function setFailed(name) {
        setStyle(name, header.styleFailed)
    }

    
    ListModel {
        id: breadcrumbsModel
        ListElement {
            style: 1
            name: "first"
            caption: "Ваш номер"
            number: 1
            last: false
            defaultHidden: false
        }
        ListElement {
            style: 0
            //            style: Header.styleFurther
            name: "confirmation"
            caption: "Ваши данные"
            number: 2
            last: false
            defaultHidden: false
        }
        ListElement {
            style: 0
            //            style: Header.styleFurther
            name: "inOut"
            caption: "Въезд-выезд"
            number: 3
            last: false
            defaultHidden: false
        }
        ListElement {
            style: 0
            //            style: Header.styleFurther
            name: "alcohol"
            caption: "Алкотест"
            number: 4
            last: false
            defaultHidden: false
        }
        ListElement {
            style: 0
            //            style: Header.styleFurther
            name: "complaints"
            caption: "Жалобы"
            number: 5
            last: true
            defaultHidden: false
        }
        ListElement {
            style: 0
            //            style: Header.styleFurther
            name: "pressure"
            caption: "Давление"
            number: 6
            last: true
            defaultHidden: true
        }
    }
    
    Component {
        id: breadcrumbsDelegate
        
        Item {
            id: breadcrumbsItem
            opacity: getOpacity()
            function getColor() {
                switch(style) {
                case header.styleCurrent:
                    return "#e9eef3"
                case header.styleFurther:
                    return "#e9eef3"
                case header.stylePassed:
                    return "#017509"
                case header.styleFailed:
                    return "#c60a20"
                }
            }
            function getTextColor() {
                switch(style) {
                case header.styleCurrent:
                    return "black"
                case header.styleFurther:
                    return "#848484"
                case header.stylePassed:
                    return "#017509"
                case header.styleFailed:
                    return "#c60a20"
                }
            }
            function getCircleTextColor() {
                switch(style) {
                case header.styleCurrent:
                    return "black"
                case header.styleFurther:
                    return "#848484"
                case header.stylePassed:
                    return "white"
                case header.styleFailed:
                    return "white"
                }
            }

            function getOpacity() {
                if (name == "pressure") {
                    if (typeof measurements == typeof undefined) {
                        return 0.0
                    } else if (!measurements.hasComplaints) {
                        return 0.0
                    } else {
                        return 1.0
                    }
                } else {
                    return 1.0
                }

            }
            function isLast() {
                if (typeof measurements == typeof undefined) {
                    if (name == "complaints" ) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    if (name == "complaints" && !measurements.hasComplaints) {
                        return true;
                    } else if (name == "pressure" && measurements.hasComplaints) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }

            height: parent.height
            implicitWidth: circle.width + circle.anchors.leftMargin + text.anchors.leftMargin +  text.width + arrRight.width + arrRight.anchors.leftMargin
            
            Rectangle {
                //                visible: name != "pressure" || measurements.hasComplaints
                opacity: getOpacity()

                id: circle
                radius: width * 0.5

                color: getColor()



                height: 27
                width: 27
                anchors.leftMargin: 24
                anchors.left: parent.left
                anchors.topMargin: 17
                anchors.top: parent.top
                Text {
                    text: number
                    anchors.centerIn: parent
                    font.family: "PTSans"
                    font.pixelSize: 11
                    color: getCircleTextColor()
                }
            }
            
            Text {
                id: text
                text: caption
                color: getTextColor()
                anchors.left: circle.right
                anchors.leftMargin: 12
                anchors.top: parent.top
                anchors.topMargin: 17
                height: 27
                verticalAlignment: Text.AlignVCenter
                font.family: "PTSans"
                font.pixelSize: 11
                font.capitalization: Font.AllUppercase
            }
            Text {
                visible: !isLast()
                id: arrRight
                color: getTextColor()
                text: ""
                anchors.left: text.right
                anchors.leftMargin: 11
                anchors.top: parent.top
                anchors.topMargin: 17
                height: 27
                verticalAlignment: Text.AlignVCenter
                font.family: "FontAwesome"
                font.pixelSize: 9
            }
        }
        
    }
    
    ListView {
        id: breadcrumbsView
        //            spacing: 23
        width: parent.width
        anchors.topMargin: 0
        spacing: 0
        orientation: Qt.LeftToRight
        anchors.top: parent.top
        anchors.left: parent.left
        model: breadcrumbsModel
        delegate: breadcrumbsDelegate
    }
    
    Rectangle {
        MouseArea {
            id: abortButtonMouseArea
            anchors.fill: parent
            onClicked: {
                startFromScratch()
            }
        }

        id: abortButton
        anchors {
            top: parent.top
            right: parent.right
            rightMargin: 23
            topMargin: 15
        }
        height: 30
        width: 175
        color: abortButtonMouseArea.pressed? "#c6520a" : "#c60a20"
        border.width: 0
        
        Item {
            id: item1
            height: parent.height
            implicitWidth: cross.width + abortText.width + abortText.anchors.leftMargin
            transformOrigin: Item.Center
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            Text {
                id: cross
                
                height: 12
                text: ""
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 0
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 12
                font.family: "FontAwesome"
                color: "white"
            }
            
            Text {
                id: abortText
                height: parent.height
                verticalAlignment: Text.AlignVCenter
                color: "white"
                font.family: "PTSans"
                font.pixelSize: 12
                text: "Прервать осмотр"
                anchors.left: cross.right
                anchors.leftMargin: 12

                
            }
        }
    }
    
    Component.onCompleted: header.setCurrent("first")
    
    
}
