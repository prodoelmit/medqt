import QtQuick 2.0
import QtQuick.Layouts 1.1

ChangeScreenButton {
    id: yesButton
    property var topColor : "#06567b";
    property var bottomColor : "#0675b5";
    gradient: Gradient {
        GradientStop {
            position: 0
            color: mouseArea.pressed ? topColor : bottomColor
        }

        GradientStop {
            position: 1.000
            color: mouseArea.pressed ? bottomColor : topColor
        }
    }
    anchors {
        right: parent.right
        rightMargin: 16
    }

    Item {
        id: labelAndIconWrapper
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        implicitWidth: labelText.width + iconText.width + iconText.anchors.leftMargin


        Text {
            id: labelText
            color: "#ffffff"
            text: yesButton.label
            anchors.verticalCenter: parent.verticalCenter
        font.family: "PTSans"
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        }

        Text {
            visible: text.length > 0
            id: iconText
            color: "#ffffff"
            text: yesButton.icon
            anchors.left: labelText.right
            anchors.leftMargin: (visible ? 16 : 0)
            anchors.verticalCenter: parent.verticalCenter
            font.family: "FontAwesome"
        font.pixelSize: 18
        verticalAlignment: Text.AlignVCenter
        }

    }


}
