#include "omronadaptor.h"
#include <iostream>
#include <QTime>
using namespace std;


OmronAdaptor::OmronAdaptor()
{
#ifdef OMRON
    m_dll = new OmronDLL("occLib.dll");


    cout << m_dll->Occ_Init(0) << endl; // 0 stands for BP monitor

    char modelList[500];
    char conList[500];

    m_dll->Occ_GetDeviceList(0, modelList, conList);
    cout << "getDeviceList: " << modelList << endl << conList << endl;
    byte deviceName[] = {0x48,0x45,0x4D,0x2D,0x31,0x30,0x34,0x30, 0x00}; // HEM-1040 .It should be null-terminated one-byte string;
    m_dll->Occ_SetConnectDeviceName(deviceName);

    cout << "openDevice: " << m_dll->Occ_OpenDevice() << endl;
    byte info[44];
//    cout << "deviceInfo: " << m_dll->Occ_GetDeviceInfo(info) << endl;
//    cout << info;
//    cout << "info:\t devicename: " << info.device_name << endl;

    cout << "setUserKbn: " << m_dll->Occ_SetUserKbn(0) << endl;
//    cout << "clearing data: " << m_dll->Occ_DataClear() << endl;

#endif


}

void OmronAdaptor::clearMemory()
{
#ifdef OMRON
    cout << "clearing data: " << m_dll->Occ_DataClear() << endl;
#endif
}

//#ifdef OMRON
int OmronAdaptor::readData(int &sys, int &dia, int &heartRate)
{
    cout << "StartDownload: " << m_dll->Occ_StartDownload() << endl;
    int i = 0;
    int download = 0;
    int sts = 0;
    QTime time;
    time.start();
    bool success = false;
    while (time.elapsed() < 5000) {
        cout <<  i++ << " querying status ";
        sts = m_dll->Occ_GetStatus();
        cout << "Occ_getStatus: " << sts << endl;
        download = m_dll->Occ_GetDownloadStatus();
        cout << " Download progress: " << download << endl;
        if (sts == 0) {
            cout << "STS = 0" << endl;
            success = true;
            break;

        } else if (sts == -1) {
            int err = m_dll->Occ_GetErrStatus();
            cout << "Occ_GetErrStatus: " << err << endl;
            break;
        }
        Sleep(500);
//        Sleep(1);
    }
    if (!success) {
        return -1;
    }
    int cnt = m_dll->Occ_GetDataCnt();
    cout << "GetDataCnt "   << cnt << endl;
    sBpRetData data;
//    byte data[1000];
    cout << "Occ_InitStrBpData" << m_dll->Occ_InitStrBpData(&data) << endl;
        cout << "Occ_SetDataCurrent" << m_dll->Occ_SetDataCurrent(cnt - 1);
        cout << "Occ_GetBpData" << m_dll->Occ_GetBpData(&data) << endl;
        cout << "data:\tsystolicBP: " << data.systolicBP << endl;
        cout << "\tdystolicBP: " << data.diastolicBP << endl;
        cout << "\tpulse:" << data.pulse << endl;
        cout << "\tbodyMovement:" << data.bodyMovementFlag << endl;
        cout << "\tdatetime:" << data.date << data.time << endl;
    sys = data.systolicBP;
    dia = data.diastolicBP;
    heartRate = data.pulse;

    if (sys < 40 || sys > 400 || dia < 40 || dia > 400 || heartRate < 20 || heartRate > 400) {
        return -1;
    } else {
        return 0;
    }
}
//#else
//int OmronAdaptor::readData(int &sys, int &dia, int &heartRate)
//{
//    sys = 120;
//    dia = 76;
//    heartRate = 63;
//    return 0;
//}
////#endif
