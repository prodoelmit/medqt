import QtQuick 2.0

import QtQuick 2.0
import QtQuick.Layouts 1.1

Rectangle {
    id: leftPane
    anchors.left: parent.left
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    visible: false
    width: 297
    color: "#424242"

    Component {
        id: leftPaneSeparator
        Rectangle {
            color: "white"
            width: leftPane.width
            height: 1
        }
    }

    ColumnLayout {
    LeftPaneText {
        visible: measurements.step >= 3
        id: leftPaneDriverText
        labels: ["Фамилия", "Имя", "Отчество", "Год рождения"]
        values: [driver.surname, driver.name, driver.patrName, driver.birthYear]
    }

    Loader {
        sourceComponent: leftPaneSeparator
        visible: measurements.step >= 4
    }

    LeftPaneText {
        visible: measurements.step >= 4
        id: leftPaneInOutText
        labels: ["Въезд/Выезд"]
        values: [(measurements.inOut ? "Выезд" : "Въезд")]
    }


    Loader {
        visible: measurements.step >= 5
        sourceComponent: leftPaneSeparator
    }

    LeftPaneText {
        visible: measurements.step >= 5

        id: leftPaneAlcoholText
        labels: ["Алкоголь (мг/л)"]
        values: [Math.floor(measurements.alcohol * 1000) / 1000
]
    }



    Loader {
        visible: measurements.step >= 6
        sourceComponent: leftPaneSeparator
    }

    LeftPaneText {
        visible: measurements.step >= 6
        id: leftPaneComplaintsText
        labels: ["Жалобы"]
        values: [(measurements.hasComplaints ? "Есть" : "Нет")]
    }

    Loader {
        sourceComponent: leftPaneSeparator
        visible: measurements.step >= 7 && measurements.hasComplaints
    }
    LeftPaneText {
        visible: measurements.step >= 7 && measurements.hasComplaints
        id: leftPanePressureText
        labels: ["Верхнее давление", "Нижнее давление", "Пульс"]
        values: [measurements.sysPressure, measurements.diaPressure, measurements.heartRate]
    }


    }
}

