import QtQuick 2.0

import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0 as Controls

Rectangle {

    property alias header: header
    property alias stack: pageContentStackView
    id: pageWrapper
    width: 1280
    height: 848
    anchors.fill: parent
    color: "#d3d8dd"


    Header {
        id: header
    }

    Connections {
        target: app
        onError: {
            pageContentStackView.currentItem.showError(errorString)
        }
    }

    Controls.StackView {
    property Component firstPage: Component {
//        id: firstPage
        FirstPage {

        }
    }

    property Component confirmationPage: Component {
//        id: confirmationPage
        DriverConfirmationPage {

        }
    }

    property Component inOutPage: Component {
//        id: inOutPage
        InOutPage {
        }
    }

    property Component bloodPressurePage: Component {
//        id: bloodPressurePage
        BloodPressurePage {

        }

    }

    property Component alcoholPage: Component {
//        id: alcoholPage
        AlcoholPage {

        }
    }

    property Component complaintsPage: Component {
//        id: complaintsPage
        ComplaintsPage {

        }
    }

    property Component finishedPage: Component {
//        id: finishedPage
        FinishedPage {

        }
    }

        anchors {
            horizontalCenter: parent.horizontalCenter
            top: header.bottom
            bottom: pageWrapper.bottom
            topMargin: 16
            bottomMargin: 16
        }
        width: pageWrapper.width

        id: pageContentStackView
        initialItem: firstPage

        pushEnter: Transition{}
        pushExit: Transition{}
        popEnter: Transition{}
        popExit: Transition{}
    }
    Component.onCompleted: console.log("Constructed driverConfirmation page");

}



