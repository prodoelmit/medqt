import QtQuick 2.0
import QtQuick.Layouts 1.1

PageContent {
    h1: "Проверка на алкоголь"
    leftPaneVisible: true
    yesButtonText: "Сохранить"
    yesButton.visible: measurements.alcoholMeasurementStatus === 2
    noButtonText: "Назад"
        Connections {
            target: noButton.mouseArea
            onClicked: {
                header.setCurrent("inOut")
                measurements.step -= 1
                pageContentStackView.pop()
            }


        }
        Connections {
            target: yesButton.mouseArea
            onClicked: {
                header.setPassed("alcohol")
                header.setCurrent("complaints")
                measurements.step += 1
                pageContentStackView.push(pageContentStackView.complaintsPage)
            }

        }

    contents: Component {

        ColumnLayout {
            anchors.centerIn: parent
//            implicitHeight: howtoText.height + readPressureButton.height + readPressureButton.anchors.topMargin + resultsGrid.height
        Text {
//            height: 140
            id: howtoText
            anchors.horizontalCenter: parent.horizontalCenter
//            anchors.top: parent.top
            text:
"1. Наденьте одноразовый мундштук на прибор
2. С силой дуйте в мундштук до характерного щелчка
3. Нажмите кнопку ОК, после появления на устройстве надписи «Ждите», нажмите кнопку «Считать показания»"
            font.pixelSize: 14
            font.family: "PTSans"
        }

        Item {
            anchors.horizontalCenter: parent.horizontalCenter
            height: 80

        YesButton {
            height: 80
            id: readAlcoholButton
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
//            anchors.top: howtoText.bottom
//            anchors.topMargin: 20
            label: "Считать показания"
            icon: {switch (measurements.alcoholMeasurementStatus) {
                case -1: return "\uf021"
                case 0: return "\uf021"
                case 1: return "\uf1eb"
                case 2: return "\uf00c"
                }
                }

            width: 300
            anchors.right: undefined
//            anchors.bottom: parent.bottom
        }
        }

        Connections {
            target: readAlcoholButton.mouseArea
            onClicked: {
                var status = measurements.alcoholMeasurementStatus
                if (status === 0 || status === 1 || status === -1)
                {
                    measurements.readDraegerData()
                }
            }
        }


        GridLayout {
            opacity: measurements.alcoholMeasurementStatus == 2
            id: resultsGrid
//            anchors.top:  readAlcoholButton.bottom
//            anchors.topMargin: 20
            anchors.horizontalCenter: parent.horizontalCenter
            columns: 2
            width: 300


        Text {
            text: "Количество алкоголя (мг/л): "
            font.pixelSize: 20
            font.family: "PTSans"
        }

        Text {
            text: Math.floor(measurements.alcohol * 1000) / 1000
            font.pixelSize: 20
            font.family: "PTSans"
            horizontalAlignment: Text.AlignRight
            Layout.fillWidth: true
        }

        }
        }



    }

}
