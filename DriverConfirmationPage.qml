import QtQuick 2.0
import QtQuick.Layouts 1.1

PageContent {



        h1: "Это вы?"

        yesButtonText: "Да, это я"
        noButtonText: "Нет, назад"
//        leftPaneVisible: true

        finishLoading: function() {
            console.log("Driver confirmation page was pushed");
            contentsLoader.item.fillValues()

        }

        Connections {
            target: noButton.mouseArea
            onClicked: {
                startFromScratch()
//                pageContentStackView.pop()
//                header.setCurrent("first")
            }

        }
        Connections {
            target: yesButton.mouseArea
            onClicked: {
                pageContentStackView.push(pageContentStackView.inOutPage)
                app.startMeasurements()
                header.setPassed("confirmation")
                header.setCurrent("inOut")
                measurements.step = 3
            }

        }

        contents: Component {

            DriverConfirmationZone {
            }
        }

}



