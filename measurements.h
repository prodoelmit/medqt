#ifndef MEASUREMENTS_H
#define MEASUREMENTS_H
#include "driver.h"
#include <QDateTime>
#include "qdebug.h"

#include <QObject>

class Measurements : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool inOut READ inOut WRITE setInOut NOTIFY inOutChanged)
    Q_PROPERTY(qreal alcohol READ alcohol WRITE setAlcohol NOTIFY alcoholChanged)
    Q_PROPERTY(int sysPressure READ sysPressure WRITE setSysPressure NOTIFY sysPressureChanged)
    Q_PROPERTY(int diaPressure READ diaPressure WRITE setDiaPressure NOTIFY diaPressureChanged)
    Q_PROPERTY(QDateTime datetime READ datetime WRITE setDatetime NOTIFY datetimeChanged)
    Q_PROPERTY(bool hasComplaints READ hasComplaints WRITE setHasComplaints NOTIFY hasComplaintsChanged)
    Q_PROPERTY(Driver* driver READ driver WRITE setDriver NOTIFY driverChanged)
    Q_PROPERTY(int heartRate READ heartRate WRITE setHeartRate NOTIFY heartRateChanged)
    Q_PROPERTY(int heartRateStatus READ heartRateStatus WRITE setHeartRateStatus NOTIFY heartRateStatusChanged)
    Q_PROPERTY(int alcoholMeasurementStatus READ alcoholMeasurementStatus WRITE setAlcoholMeasurementStatus NOTIFY alcoholMeasurementStatusChanged)
    Q_PROPERTY(int step READ step WRITE setStep NOTIFY stepChanged)

    int m_step;
    bool m_inOut;
    qreal m_alcohol;
    int m_sysPressure;
    int m_diaPressure;
    QDateTime m_datetime;
    bool m_hasComplaints;
    Driver* m_driver;
    int m_heartRate;

    int m_heartRateStatus;

    int m_alcoholMeasurementStatus;


public:
    explicit Measurements(Driver* driver);

    void clear();

    QString serialize();
    Q_INVOKABLE bool readOmronData();
    Q_INVOKABLE bool readDraegerData();
    Q_INVOKABLE void finish();

    bool inOut() const;

    qreal alcohol() const;

    int sysPressure() const;

    int diaPressure() const;

    QDateTime datetime() const;

    bool hasComplaints() const;

    Driver* driver() const;

    int heartRate() const;

    Q_INVOKABLE void startMeasurements();

    int heartRateStatus() const;

    int alcoholMeasurementStatus() const;

    int step() const
    {
        return m_step;
    }

signals:

    void error(QString s);

    void inOutChanged(bool     inOut);

    void alcoholChanged(qreal alcohol);

    void sysPressureChanged(int sysPressure);

    void diaPressureChanged(int diaPressure);

    void datetimeChanged(QDateTime datetime);

    void hasComplaintsChanged(bool hasComplaints);

    void driverChanged(Driver* driver);

    void heartRateChanged(int heartRate);

    void heartRateStatusChanged(int heartRateStatus);

    void alcoholMeasurementStatusChanged(int alcoholMeasurementStatus);

    void stepChanged(int step);

public slots:
    void setInOut(bool inOut);
    void setAlcohol(qreal alcohol);
    void setSysPressure(int sysPressure);
    void setDiaPressure(int diaPressure);
    void setDatetime(QDateTime datetime);
    void setHasComplaints(bool hasComplaints);
    void setDriver(Driver* driver);
    void setHeartRate(int heartRate);
    void setHeartRateStatus(int heartRateStatus);
    void setAlcoholMeasurementStatus(int alcoholMeasurementStatus);
    void setStep(int step)
    {
        qDebug() << step;
        if (m_step == step)
            return;

        m_step = step;
        emit stepChanged(step);
    }
};

#endif // MEASUREMENTS_H
