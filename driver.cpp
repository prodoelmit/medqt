#include "driver.h"

Driver::Driver()
    : QObject()
{

}

Driver::~Driver()
{
    int a = 0;

}

QString Driver::surname() const
{
    return m_surname;
}

QString Driver::name() const
{
    return m_name;
}

QString Driver::patrName() const
{
    return m_patrName;
}

int Driver::birthYear() const
{
    return m_birthYear;
}

int Driver::persNumber() const
{
    return m_persNumber;
}

QString Driver::avatar() const
{
    return m_avatar;
}

void Driver::setSurname(QString surname)
{
    if (m_surname == surname)
        return;

    m_surname = surname;
    emit surnameChanged(surname);
}

void Driver::setName(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(name);
}

void Driver::setPatrName(QString patrName)
{
    if (m_patrName == patrName)
        return;

    m_patrName = patrName;
    emit patrNameChanged(patrName);
}

void Driver::setBirthYear(int birthYear)
{
    if (m_birthYear == birthYear)
        return;

    m_birthYear = birthYear;
    emit birthYearChanged(birthYear);
}

void Driver::setPersNumber(int persNumber)
{
    if (m_persNumber == persNumber)
        return;

    m_persNumber = persNumber;
    emit persNumberChanged(persNumber);
}

void Driver::setAvatar(QString avatar)
{
    if (m_avatar == avatar)
        return;

    m_avatar = avatar;
    emit avatarChanged(avatar);
}
