#ifndef OMRONDLL_H
#define OMRONDLL_H
#include "pdll.h"

#pragma pack(push, 1)
struct sBpRetData {
    INT32 systolicBP;
    INT32 diastolicBP;
    INT32 pulse;
    INT32 measurementTiming;
    INT32 numberInRow;
    INT32 settingNumber;
    INT32 memoFlag;
    INT32 bodyMovementFlag;
    INT32 irregularHeartBeatFlag;
    INT32 dataCategory;
    INT32 dataSerialNumber;
    INT32 user;
    char date[11];
    char time[9];
    INT32 hideFlag;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct sDeviceInfo {
    char device_name[10];
    char revision[10];
    char serial[16];
    INT32 world;
    INT32 target;
};
#pragma pack(pop)

class VirtualOmron {
public:
    virtual int Occ_Init(int mode) = 0;
    virtual int Occ_GetDeviceList(int mode, char* modelList, char* conList) = 0;
    virtual int Occ_SetConnectDeviceName(byte* modellist) = 0;
    virtual int Occ_OpenDevice() = 0;
    virtual int Occ_SetUserKbn(int userkbn) = 0;
    virtual int Occ_StartDownload() = 0;
    virtual int Occ_GetDownloadStatus() = 0;
    virtual int Occ_GetStatus() = 0;
    virtual int Occ_InitStrBpData(sBpRetData* bp) = 0;
    virtual int Occ_GetDataCnt() = 0;
    virtual int Occ_SetDataCurrent(int number) = 0;
    virtual int Occ_GetBpData(sBpRetData* retData) = 0;
    virtual int Occ_End() = 0;
    virtual int Occ_GetErrStatus() = 0;
    virtual int Occ_GetConnectMode() = 0;
    virtual int Occ_GetDeviceInfo(byte* info) = 0;
    virtual int Occ_DataClear() = 0;



};

class OmronDLL: public PDLL ,public VirtualOmron
{
    DECLARE_CLASS(OmronDLL)
    DECLARE_FUNCTION1(int, Occ_Init, int)
    DECLARE_FUNCTION3(int, Occ_GetDeviceList, int, char*, char*)
    DECLARE_FUNCTION1(int, Occ_SetConnectDeviceName, byte*)
    DECLARE_FUNCTION0(int, Occ_OpenDevice)
    DECLARE_FUNCTION1(int, Occ_SetUserKbn, int)
    DECLARE_FUNCTION0(int, Occ_StartDownload)
    DECLARE_FUNCTION0(int, Occ_GetDownloadStatus)
    DECLARE_FUNCTION0(int, Occ_GetStatus)
    DECLARE_FUNCTION1(int, Occ_InitStrBpData, sBpRetData*)
    DECLARE_FUNCTION0(int, Occ_GetDataCnt)
    DECLARE_FUNCTION1(int, Occ_SetDataCurrent, int)
    DECLARE_FUNCTION1(int, Occ_GetBpData, sBpRetData*)
    DECLARE_FUNCTION0(int, Occ_End)
    DECLARE_FUNCTION0(int, Occ_GetErrStatus)
    DECLARE_FUNCTION0(int, Occ_GetConnectMode)
    DECLARE_FUNCTION1(int, Occ_GetDeviceInfo, byte*)
    DECLARE_FUNCTION0(int, Occ_DataClear)



};

#endif // OMRONDLL_H
