#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "app.h"
#include <QTimer>

int main(int argc, char *argv[])
{
    App a(argc, argv);


    QTimer::singleShot(0, &a, SLOT(run()));

    return a.exec();
}
