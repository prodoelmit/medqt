import QtQuick 2.6
import QtQuick.Layouts 1.1

Rectangle {
    anchors.fill: parent
ListModel {
    id: labels
    ListElement {
        v: "Фамилия"
    }
    ListElement {
        v: "Имя"
    }
    ListElement {
        v: "Отчество"
    }
    ListElement {
        v: "Дата рождения"
    }
    ListElement {
        v: "Табельный номер"
    }
}
ListModel {
    id: values
//    Component.onCompleted: {
//        values.append({"v": driver.name})
//        values.append({"v": driver.surname})
//        values.append({"v": driver.patrName})
//        values.append({"v": driver.birthYear})
//        values.append({"v": driver.persNumber})

//    }

}
function fillValues() {
        values.append({"v": driver.surname})
        values.append({"v": driver.name})
        values.append({"v": driver.patrName})
        values.append({"v": driver.birthYear.toString()})
        values.append({"v": driver.persNumber.toString()})
}

//ListView {
//    anchors.fill: parent
//    model: values
//    delegate: Text {
//        text: v
//    }


//}

Row {
    id: row
    anchors.centerIn:  parent
//    Column {
//        height: 310
//        width: 310
//        Image {
//            height: parent.height
//            width: parent.width
//            source: driver.avatar
//        }
//    }
    Column {
        height: 310
        width: 310

        ListView {
            interactive: false
            id: listviewLabels
            width: parent.width
            height: parent.height
            orientation: ListView.Vertical
            model: labels
            delegate:  Text {
                color: "#4f5762"

                    text: v + ":"
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    width: listviewLabels.width - 10
                    anchors.right: listviewLabels.right
                    anchors.rightMargin: 6
                    height: listviewLabels.height / 5
                    font.pixelSize: 15
                    font.family: "PTSans"
                }
            }
    }
    Column {
        height: 310
        width: 310
        ListView {
            interactive: false
            id: listviewValues
            width: parent.width
            height: parent.height
            orientation: ListView.Vertical
            model: values
            delegate:  Text {
                color: "#011d2a"

                    text: v
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    width: listviewValues.width - 10
                    anchors.right: listviewValues.right
                    anchors.rightMargin: 6
                    height: listviewValues.height / 5
                    font.pixelSize: 14
                    font.family: "PTSans"
                }
            }
    }
}


}


