import QtQuick 2.0
import QtQuick.Layouts 1.1

Rectangle {
    Connections {
        target: window
        onStartedFromScratch: removeAllDigits()
    }

    property alias driverId: tabIdEdit.text
                function addDigit(n) {
                    tabIdEdit.text = tabIdEdit.text + n
                }
                function removeDigit() {
                    tabIdEdit.text = tabIdEdit.text.slice(0, tabIdEdit.text.length - 1)
                }
                function removeAllDigits() {
                    tabIdEdit.text = ""
                }
    id: tabEditZone
    width: 557
    color: "#ffffff"
    
    anchors.verticalCenter: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter
    implicitHeight: tabEditGrid.height + tabIdEditWrapper.height + tabEditGrid.anchors.topMargin
    
    GridLayout {
        id: tabEditGrid
        width: parent.width
        anchors.top: tabIdEditWrapper.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.topMargin: 32
        columnSpacing: 36
        rowSpacing: 36
        rows: 3
        columns: 4
        TabIdNumberButton {
            number: 1
        }
        TabIdNumberButton {
            number: 2
        }
        TabIdNumberButton {
            number: 3
        }
        TabIdActionButton {
            icon: ""
            label: "Удалить"
            inactiveColor: "#02808f"
            activeColor: "#02398f"
            action: removeDigit
//            MouseArea {
//                id: mouseArea
//                anchors.fill: parent
//                onClicked: removeDigit()
//            }
        }
        TabIdNumberButton {
            number: 4
        }
        TabIdNumberButton {
            number: 5
        }
        TabIdNumberButton {
            number: 6
        }
        TabIdActionButton {
            icon: ""
            label: "Удалить всё"
            inactiveColor: "#025b66"
            activeColor: "#022966"
            action: removeAllDigits
//            MouseArea {
//                id: mouseArea
//                anchors.fill: parent
//                onClicked: removeAllDigits()
//            }
        }
        TabIdNumberButton {
            number: 7
        }
        TabIdNumberButton {
            number: 8
        }
        TabIdNumberButton {
            number: 9
        }
        TabIdNumberButton {
            number: 0
        }
    }
    
    Rectangle {
        id: tabIdEditWrapper
        color: darkGrayColor
        radius: 12
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 0
        border.color: grayColor
        border.width: 2
        width: 557
        height: 54
        
        
        TextEdit {
            id: tabIdEdit
            height: 30
            text: ""
            anchors.rightMargin: 0
            anchors.leftMargin: 0
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.left: parent.left
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 30
        }
    }
}
