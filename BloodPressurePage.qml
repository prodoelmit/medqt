import QtQuick 2.0
import QtQuick.Layouts 1.1

PageContent {
    h1: "Измерение артериального давления"
    leftPaneVisible: true
    yesButtonText: "Сохранить"
    yesButton.visible: measurements.heartRateStatus == 2
    noButtonText: "Назад"
        Connections {
            target: noButton.mouseArea
            onClicked: {
                header.setCurrent("inOut")
                measurements.step = measurements.step - 1
                pageContentStackView.pop()
            }

        }
        Connections {
            target: yesButton.mouseArea
            onClicked: {
                header.setPassed("pressure")
                measurements.step += 1
                pageContentStackView.push(pageContentStackView.finishedPage)
            measurements.finish()
            }

        }

    contents: Component {

        ColumnLayout {
            anchors.centerIn: parent
//            implicitHeight: howtoText.height + readPressureButton.height + readPressureButton.anchors.topMargin + resultsGrid.height
        Text {
//            height: 140
            id: howtoText
            anchors.horizontalCenter: parent.horizontalCenter
//            anchors.top: parent.top
            text:
"1. Проверьте, включен ли тонометр
2. Установите на тонометре режим «А», нажав синюю кнопку с изображением фигурки человека
3. Проденьте руку в манжету
4. Сядьте прямо - так, чтобы середина манжеты находилась примерно на уровне сердца
5. Нажмите на синюю кнопку \"Старт\" на тонометре
6. После завершения измерения нажмите \"Считать показания\""
            font.pixelSize: 14
            font.family: "PTSans"
        }

        Item {
            anchors.horizontalCenter: parent.horizontalCenter
//            anchors
//            anchors.top: howtoText.bottom
//            anchors.topMargin: 20
//            anchors.bottomMargin: 20
            height: 80
            width: 300
        YesButton {

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            height: 80
//            implicitHeight: 40
            id: readPressureButton
            label: "Считать показания"
            icon: {switch (measurements.heartRateStatus) {
                    case -1: return "\uf21e"
                  case 0: return "\uf21e"
                  case 1: return "\uf1eb"
                  case 2: return"\uf00c"
                  }}

            width: 300
            anchors.right: undefined
//            anchors.bottom: parent.bottom
        }
        }
            Connections {
                target: readPressureButton.mouseArea
                onClicked: {
                    if (measurements.heartRateStatus === 0 ||
                            measurements.heartRateStatus === -1)
                    {
                        measurements.readOmronData()
                    }
                }
            }


        GridLayout {
            opacity: measurements.heartRateStatus == 2
            id: resultsGrid
//            anchors.top:  readPressureButton.bottom
//            anchors.topMargin: 20
            anchors.horizontalCenter: parent.horizontalCenter
            columns: 2
            width: 300


        Text {
            text: "Верхнее значение:"
            font.pixelSize: 20
            font.family: "PTSans"
        }

        Text {
            text: measurements.sysPressure
            font.pixelSize: 20
            font.family: "PTSans"
            horizontalAlignment: Text.AlignRight
            Layout.fillWidth: true
        }

        Text {
            text: "Нижнее значение:"
            font.pixelSize: 20
            font.family: "PTSans"
        }

        Text {
            text: measurements.diaPressure
            font.pixelSize: 20
            horizontalAlignment: Text.AlignRight
            Layout.fillWidth: true
            font.family: "PTSans"
        }

        Text {
            text: "Пульс:"
            font.pixelSize: 20
            font.family: "PTSans"
        }

        Text {
            text: measurements.heartRate
            font.pixelSize: 20
            font.family: "PTSans"
            horizontalAlignment: Text.AlignRight
            Layout.fillWidth: true
        }


        }
        }



    }

}
