#ifndef OMRONADAPTOR_H
#define OMRONADAPTOR_H
#ifdef OMRON
#include "omrondll.h"
#endif


class OmronAdaptor
{
public:
    OmronAdaptor();
    void clearMemory();
    int readData(int& sys, int& dia, int& heartRate);
private:
#ifdef OMRON
    OmronDLL* m_dll;
#endif
};

#endif // OMRONADAPTOR_H
