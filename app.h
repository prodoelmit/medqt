#ifndef APP_H
#define APP_H
#include "driver.h"
#include <QGuiApplication>
#include "measurements.h"
#include "omronadaptor.h"

#include "apiadaptor.h"
#include "draegeradaptor.h"
#include <QQmlApplicationEngine>



class App: public QGuiApplication
{
    Q_OBJECT
public:
    App(int& argc, char** argv);

    Q_INVOKABLE void fetchDriver(int id);
    Q_INVOKABLE void startMeasurements();
    Q_INVOKABLE void abortMeasurements();

    OmronAdaptor *omron() const;

    void startFromScratch();

    ApiAdaptor *api() const;

    int currentAlcoholTestsCounter() const;

    DraegerAdaptor *draeger() const;

    void setCurrentAlcoholTestsCounter(int currentAlcoholTestsCounter);

public slots:
    void run();

signals:
    void driverFetched();
    void error(QString errorString);


private:
    QQmlApplicationEngine* m_engine;
    Driver* m_driver;
    Measurements* m_measurements;
    OmronAdaptor* m_omron;
    DraegerAdaptor* m_draeger;
    int m_currentAlcoholTestsCounter;
    ApiAdaptor* m_api;
};



#define app dynamic_cast<App*>(qApp->instance())

#endif // APP_H
