import QtQuick 2.0

PageContent {
    h1: "У вас есть жалобы на самочувствие?"
    yesButtonText: ""
    noButtonText: "Назад"

    leftPaneVisible: true

    Connections {
        target: noButton.mouseArea
        onClicked: {
            header.setCurrent("alcohol")
            measurements.step -= 1
            pageContentStackView.pop()
        }
    }



    contents: Component {
Item {

    anchors.fill: parent


    Column {
        anchors.centerIn: parent
        width: 364
        spacing: 27
        Row {
            width: parent.width

            YesButton  {
                width: parent.width
                height: 66
                id: noComplaintsButton
                anchors {
                    right: undefined
                    rightMargin: 0
                    bottom: undefined
                    bottomMargin: 0
                }
                label: "Нет"
                icon: ""

            }

            Connections {
                target: noComplaintsButton.mouseArea
                onClicked: {
                    measurements.hasComplaints = false
                    measurements.step += 2
                    measurements.finish()

                    header.setPassed("complaints")

                    stack.push(pageContentStackView.finishedPage)

                }
            }
        }
        Row {
            width: parent.width
            NoButton  {
                width: parent.width
                id: haveComplaintsButton
                height: 66
                anchors {
                    left: undefined
                    leftMargin: 0
                    bottom: undefined
                    bottomMargin: 0
                }
                label: "Да"
                icon: ""

            }
            Connections {
                target: haveComplaintsButton.mouseArea
                onClicked: {
                    measurements.hasComplaints = true
                    measurements.step += 1

//                    measurements.finish()
                    header.setPassed("complaints")
                    header.setCurrent("pressure")
                    stack.push(pageContentStackView.bloodPressurePage)

                }
            }

//            Rectangle {
//                id: inButton
//                width: parent.width
//                height: 66
//    gradient: Gradient {
//        GradientStop {
//            position: 1
//            color: "#eaeaea"
//        }

//        GradientStop {
//            position: 0
//            color: "#ffffff"
//        }
//    }
//    border.color: "#d5d5d5";
//            }

        }
    }
}
    }

}
