import QtQuick 2.0
import QtQuick.Layouts 1.1

Rectangle {
    id: tabIdButton
    width: 110
    height: 110
    color: "#ffffff"
    radius: 12
    border.color: "#c5cad0"
}
