import QtQuick 2.0
import QtQuick.Layouts 1.1

TabIdButton {
    property int number: 1
    property var inactiveColor: "#ffffff"
    property var activeColor: "#eaeaea"
    color: mouseArea.pressed ? activeColor : inactiveColor

    Text {
        id: numberText
        text: parent.number
        font.family: "PTSans"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.fill: parent
        font.pixelSize: 40
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: addDigit(number)
    }



}
