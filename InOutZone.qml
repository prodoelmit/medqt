import QtQuick 2.0

Item {

    anchors.fill: parent


    Column {
        anchors.centerIn: parent
        width: 364
        spacing: 27
        Row {
            width: parent.width

            YesButton  {
                id: outButton
                width: parent.width
                height: 66
                anchors {
                    right: undefined
                    rightMargin: 0
                    bottom: undefined
                    bottomMargin: 0
                }
                label: "Выезд"

            }
                Connections {
                    target: outButton.mouseArea
                    onClicked: {
                        pageContentStackView.push(pageContentStackView.alcoholPage)
                        header.setPassed("inOut")
//                        header.setCurrent("pressure")
                        header.setCurrent("alcohol")
                        measurements.inOut = true
                measurements.step += 1
                    }
                }
        }
        Row {
            width: parent.width
            NoButton  {
                id: inButton
                width: parent.width
                height: 66
                anchors {
                    left: undefined
                    leftMargin: 0
                    bottom: undefined
                    bottomMargin: 0
                }
                label: "Въезд"
                Connections {
                    target: inButton.mouseArea
                    onClicked: {
                        pageContentStackView.push(pageContentStackView.alcoholPage)
                        header.setPassed("inOut")
                        header.setCurrent("alcohol")
                        measurements.inOut = false
                measurements.step += 1
                    }
                }

            }

//            Rectangle {
//                id: inButton
//                width: parent.width
//                height: 66
//    gradient: Gradient {
//        GradientStop {
//            position: 1
//            color: "#eaeaea"
//        }

//        GradientStop {
//            position: 0
//            color: "#ffffff"
//        }
//    }
//    border.color: "#d5d5d5";
//            }

        }
    }
}
