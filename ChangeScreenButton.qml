import QtQuick 2.0
import QtQuick.Layouts 1.1

Rectangle {
    property alias mouseArea: mouseArea
    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    width: 220
    height: 42
    anchors {
        bottom: parent.bottom
        bottomMargin: 16
    }
    border.color: "#d5d5d5"
    border.width: 1

    
    property string label: "Вперед"
    property string icon: ""

    visible: (label.length > 0)
    

    
    
}
