#include "app.h"
#include <QTimer>
#include <QQmlContext>
#include "qdebug.h"

void MessageHandler(QtMsgType type, const QMessageLogContext & context, const QString & msg)
{

    QDateTime dateTime(QDateTime::currentDateTime());

    QString timeStr(dateTime.toString("dd-MM-yyyy HH:mm:ss:zzz"));
    QString contextString(QString("(%1, %2)").arg(context.file).arg(context.line));

    QFile outFile("log.log");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);

    QTextStream stream(&outFile);
    stream << timeStr << " " << contextString << ": " << msg << endl << endl;

}

App::App(int &argc, char **argv)
    : QGuiApplication(argc, argv)
    , m_driver(0)
    , m_measurements(0)
{
//    qInstallMessageHandler(&MessageHandler);

    m_driver = new Driver();
    m_measurements = new Measurements(m_driver);
    m_omron = new OmronAdaptor;
    m_omron->clearMemory();
    m_draeger = new DraegerAdaptor;
    m_api = new ApiAdaptor;
    m_draeger->fetchResults();
    m_currentAlcoholTestsCounter = m_draeger->getLastMeasurementId();
    qDebug() << "Current alcohol tests counter: " << m_currentAlcoholTestsCounter;

}

void App::fetchDriver(int id)
{
    qDebug() << id;
    m_driver = new Driver();
    m_driver->setParent(this);
    m_driver->setPersNumber(id);
    connect(m_api, &ApiAdaptor::error, [this](QString s) {
        emit error(s);
    });
    connect(m_api, &ApiAdaptor::driverInfoFetched, [this]() {
        m_engine->rootContext()->setContextProperty("driver", m_driver);
        emit driverFetched();
    });
    m_api->fetchDriverInfo(m_driver);
//    m_driver->setName("Yuriy");
//    m_driver->setSurname("Dynnikov");
//    m_driver->setPatrName("Alexeevich");
//    m_driver->setAvatar("http://dev.morodeer.ru/avatar.png");
//    m_driver->setBirthYear(1901);
//    m_driver->setPersNumber(id);
}

void App::startMeasurements()
{
    if (m_measurements) {
        abortMeasurements();
    }
    m_omron->clearMemory();
    m_measurements = new Measurements(m_driver);
    connect(m_measurements, &Measurements::error, [this](QString s) {
        emit error(s);
    });
    m_measurements->startMeasurements();
    m_engine->rootContext()->setContextProperty("measurements", m_measurements);
}

void App::abortMeasurements()
{
//    if (m_driver) {
//        delete m_driver;
//    m_engine->rootContext()->setContextProperty("driver", 0);
//    }
//    m_driver = 0;
    if (m_measurements) {
        delete m_measurements;
    m_engine->rootContext()->setContextProperty("measurements", 0);
    }
    m_measurements = 0;

}

void App::run()
{
    m_engine = new QQmlApplicationEngine;
    m_engine->load(QUrl(QStringLiteral("qrc:/main.qml")));
    m_engine->rootContext()->setContextProperty("app", this);

}

void App::setCurrentAlcoholTestsCounter(int currentAlcoholTestsCounter)
{
    m_currentAlcoholTestsCounter = currentAlcoholTestsCounter;
}

DraegerAdaptor *App::draeger() const
{
    return m_draeger;
}

int App::currentAlcoholTestsCounter() const
{
    return m_currentAlcoholTestsCounter;
}

ApiAdaptor *App::api() const
{
    return m_api;
}

OmronAdaptor *App::omron() const
{
    return m_omron;
}

void App::startFromScratch()
{
    QTimer::singleShot(5000, [this](){
        QMetaObject::invokeMethod(m_engine->rootObjects()[0],"startFromScratch");
    });
}
