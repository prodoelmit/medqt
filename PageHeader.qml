import QtQuick 2.0

Item {
    property alias h: headerText.text
    width: parent.width
    height: 50

    Text {
        id: headerText
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 26
        color: "Blue"
        horizontalAlignment: Text.AlignHCenter
    }

}
