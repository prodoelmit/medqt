#include "draegeradaptor.h"
#include "qdebug.h"
#include <Windows.h>
DraegerAdaptor::DraegerAdaptor()
    : m_port(0)
{


}

bool DraegerAdaptor::startPCMode()
{
    QString str = "0142000000430001000105c6";
    QString r = sendData(str, 23, 5000);

    if (r == "0142000000430001000105c60142000000430000001910") {
        qDebug() << "PC Mode succesfully initialized";
        return true;
    } else {
        qDebug() << "Error initalizing PC Mode.\nResponse from port: " << r;
        return false;
    }
}

bool DraegerAdaptor::initComPort()
{
    if (m_port && m_port->isOpen()) {
        return true;
    }
    QList<QSerialPortInfo> infos = QSerialPortInfo::availablePorts();
    QSerialPortInfo draegerInfo;
    foreach (QSerialPortInfo info, infos) {
        qDebug() << info.description() << info.portName();
        if (info.description().contains("Draeger")) {
            draegerInfo = info;
        }
    }

    if (!draegerInfo.description().contains("Draeger")) {
        qWarning("No serial port with \"Draeger\" in its description found");
    }

    m_port = new QSerialPort(draegerInfo);
    m_port->setBaudRate(57600);
    if (!m_port->open(QSerialPort::ReadWrite)) {
        qDebug() << "Error opening com port: " << m_port->errorString();
        return false;
    } else {
        qDebug() << "Com port succesfully initialized with device " << infos.first().description();
        return true;
    }

}

bool DraegerAdaptor::endPCMode()
{
    QString str = "014200000043000100008cd7";
    QString r = sendData(str, 21, 5000);


    if (r == "014200000043000100008cd7014200000043000000") {
        qDebug() << "PC Mode succesfully closed";
        return true;
    } else {
        qDebug() << "Error closing PC Mode.\nResponse from port:\t\t " << r;
        qDebug() << "Should be\t\t\t  \"014200000043000100008cd7014200000043000000\"";
        return false;
    }
}

bool DraegerAdaptor::readMeasurements(int measurementsCount)
{
    QString str = "0142000000050004000a000000150b";
    QString r = sendData(str, 24, 5000);
    m_measurements.clear();

    if (r == "0142000000050004000a000000150b014200000005000401") {
        qDebug() << "Succesfully sent readMeasurements request";
        qDebug() << "Response: " << r;
    } else {
        qDebug() << "Error requesting readMeasurements\nResponse: " << r;
        return false;
    }

    QString results;
    results = readData(262, 5000);
    qDebug() << "Results: " << results;
    int counter = 0;
    while (measurementsCount > counter) {
        QString slice = results.mid((counter % 10) * 52, 52);
        m_measurements << DraegerMeasurement(slice);
        qDebug() << counter << "\t" << m_measurements.last().inspect();
        counter++;
        if (counter % 10 == 0 && measurementsCount > counter) {
//            QString repeatStr = "0142000000050004000a000a0065f6";
            QString counterString = QString::number(counter,16);
            if (counterString.length() == 1) {
                counterString = counterString.prepend('0');
            }
            QString repeatStr = "0142000000050004000a00" + counterString + "00";
            appendCRC(repeatStr);
            qDebug() << "Sending repeatStr: " << repeatStr;
            QString repeatR = sendData(repeatStr, 24, 5000);
            if (repeatR.startsWith(repeatStr)) {
                qDebug() << "Succesfully sent repeatReadMesurements request";
                results  = readData(262, 5000);
            } else {
                qDebug() << "Error requesting one more page of measurements\nResponse: " << repeatR;
                break;
            }
        }
    }

    qDebug() << "Reading measurements completed";
    int c = 0;
    foreach (DraegerMeasurement s, m_measurements) {
        qDebug() << c++ << "\t" <<  s.inspect();
    }
    return true;

}

int DraegerAdaptor::readNumberOfTests()
{
    QString str = "014200000006000000f968";
    QString r = sendData(str, 27, 5000);

    if (r.startsWith( "014200000006000000f968014200000006000500" )) {
        qDebug() << "Succesfully read number of test and some more data";
        qDebug() << "Number of tests = " << r.mid(42,2).toInt(nullptr, 16);
        return r.mid(42,2).toInt(nullptr, 16);
    } else {
        qDebug() << "Error reading number of test results";
        return -1;
    }

}

int DraegerAdaptor::getLastMeasurementId()
{
    return m_measurements.last().id();
}

QPair<int, qreal> DraegerAdaptor::readLastAlcoholValue()
{

    QPair<int, qreal> errorPair = qMakePair(-1, -1);
    if (!initComPort()) {
        qDebug() << "error initializing com port" << m_port->errorString();
        return errorPair;
    }
    if (!startPCMode())
        return errorPair;
    int n = readNumberOfTests();
    if (n == -1) {
        qDebug() << "number of test equals -1";
        return errorPair;
    }
    if (!readMeasurements(n))
        return errorPair;

    endPCMode();

    DraegerMeasurement m = m_measurements.last();
    if (!m.passed()) {
        qDebug() << "Didn't pass!";
        return errorPair;
    }
    int id = m.id();
    qreal alcohol = m.alcohol();

    qDebug() << "readLastAlcoholValue(): " << id << alcohol;
    return qMakePair(id, alcohol);

}

QString DraegerAdaptor::sendData(QString hex, int expectedLength, int timeoutMsecs)
{
    m_port->clear();

    QByteArray ba;
    ba = QByteArray::fromHex(hex.toLatin1());

    m_port->write(ba);
    QTime timer;
    timer.start();
    QByteArray resultBA;
    while (timer.elapsed() < timeoutMsecs && resultBA.length() < expectedLength) {
        m_port->waitForReadyRead(timeoutMsecs);

        resultBA.append(m_port->read(expectedLength - resultBA.length()));
    }
    return resultBA.toHex();

}

QString DraegerAdaptor::readData(int expectedLength, int timeoutMsecs)
{
    QTime timer;
    timer.start();
    QByteArray resultBA;
    while (timer.elapsed() < timeoutMsecs && resultBA.length() < expectedLength) {
        m_port->waitForReadyRead(timeoutMsecs);

        resultBA.append(m_port->read(expectedLength - resultBA.length()));
    }
    return resultBA.toHex();
}

void DraegerAdaptor::fetchResults()
{
    initComPort();
    startPCMode();
    int n = readNumberOfTests();
    readMeasurements(n);
    endPCMode();
}

quint16 makeCrc16Kermit(const QByteArray &data)
{
    quint16     valuehex = 0;
    quint16     CRC = 0;
    int         size = data.size();
    CRC = 0;

    for(int i=0; i<size; ++i) {
        valuehex = ((static_cast<quint8>(data[i]) ^ CRC) & 0x0fu) * 0x1081;
        CRC >>= 4;
        CRC ^= valuehex;
        valuehex = ((static_cast<quint8>(data[i]) >> 4) ^ (CRC & 0x00ffu)) & 0x0fu;
        CRC >>= 4;
        CRC ^= (valuehex * 0x1081u);
    }
    quint16 ret = ( (CRC & 0x00ffu) << 8) | ((CRC & 0xff00u) >> 8);
    return ret;
}
void DraegerAdaptor::appendCRC(QString &str)
{
    QByteArray ba = QByteArray::fromHex(str.toLatin1());
    quint16 hash = makeCrc16Kermit(ba);
    QString hashString = QString::number(hash, 16);
    while (hashString.length() < 4) {
        hashString = "0" + hashString;
    }
    str = str.append(hashString);
}


